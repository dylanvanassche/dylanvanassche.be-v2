<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8" />
  <title property="foaf:name schema:name">Continuous generation of versioned collection&#39;s members with RML and LDES</title>
  <link rel="stylesheet" media="screen" href="styles/screen.css" />
  <link rel="stylesheet" media="print"  href="styles/print.css" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  
  <meta name="citation_title" content="Continuous generation of versioned collection's members with RML and LDES">
  <meta name="citation_author" content="Dylan Van Assche" />
  <meta name="citation_author" content="Sitt Min Oo" />
  <meta name="citation_author" content="Julián Andrés Rojas Melendez" />
  <meta name="citation_author" content="Pieter Colpaert" />
  
  <meta name="citation_publication_date" content="2022/03/15" />
</head>

<body prefix="dctypes: http://purl.org/dc/dcmitype/ pimspace: http://www.w3.org/ns/pim/space# rsa: http://www.w3.org/ns/auth/rsa# cert: http://www.w3.org/ns/auth/cert# wgs: http://www.w3.org/2003/01/geo/wgs84_pos# biblio: http://purl.org/net/biblio# bibo: http://purl.org/ontology/bibo/ book: http://purl.org/NET/book/vocab# ov: http://open.vocab.org/terms/ doap: http://usefulinc.com/ns/doap# dbr: http://dbpedia.org/resource/ dbp: http://dbpedia.org/property/ sio: http://semanticscience.org/resource/ opmw: http://www.opmw.org/ontology/ deo: http://purl.org/spar/deo/ doco: http://purl.org/spar/doco/ cito: http://purl.org/spar/cito/ fabio: http://purl.org/spar/fabio/ solid: http://www.w3.org/ns/solid/terms# acl: http://www.w3.org/ns/auth/acl# dio: https://w3id.org/dio# lsc: http://linkedscience.org/lsc/ns#" typeof="schema:CreativeWork sioc:Post prov:Entity lsc:Research">
  <header>
  <h1 id="continuous-generation-of-versioned-collections-members-with-rml-and-ldes">Continuous generation of versioned collection’s members with RML and LDES</h1>

  <ul id="authors">
    <li><a rev="lsc:participatesIn" property="foaf:maker schema:creator schema:author schema:publisher" href="https://dylanvanassche.be/" typeof="foaf:Person schema:Person" resource="https://dylanvanassche.be/#me">Dylan Van Assche</a></li>
    <li><a rev="lsc:participatesIn" property="foaf:maker schema:creator schema:author schema:publisher" href="https://data.knows.idlab.ugent.be/person/minoo/" typeof="foaf:Person schema:Person" resource="https://data.knows.idlab.ugent.be/person/minoo/#me">Sitt Min Oo</a></li>
    <li><a rev="lsc:participatesIn" property="foaf:maker schema:creator schema:author schema:publisher" href="https://julianrojas.org/" typeof="foaf:Person schema:Person" resource="https://julianrojas.org/#me">Julián Andrés Rojas Melendez</a></li>
    <li><a rev="lsc:participatesIn" property="foaf:maker schema:creator schema:author schema:publisher" href="https://pietercolpaert.be/" typeof="foaf:Person schema:Person" resource="https://pietercolpaert.be/#me">Pieter Colpaert</a></li>
  </ul>

  <ul id="affiliations">
    <li id="idlab">IDLab,
          Department of Electronics and Information Systems,
          Ghent University – imec</li>
  </ul>

  <section id="abstract" inlist="" rel="schema:hasPart" resource="#abstract">
<div datatype="rdf:HTML" property="schema:description">
      <h2 property="schema:name">Abstract</h2>
      <!-- Context      -->
      <p>When evolving datasets are used to generate a knowledge graph,
is usually challenging to keep the graph synchronized in a timely manner
when changes occur in the source data.
<!-- Need         -->
Current approaches fully regenerate a knowledge graph
in such cases, which may be time consuming
depending on the type, size and update frequency of the data source(s).
<!-- Task         -->
We propose a continuous knowledge graph generation approach
that can be applied on different types of data sources.
We describe continuously updating knowledge graph versions
represented as a Linked Data Events Stream,
and use an RML processor for differential knowledge generation.
<!-- Object       -->
In this paper,
we present our approach and validate it
on different types of data
such as public transit timetables, bike sharing, and weather data.
<!-- Findings     -->
By describing entities with unique, immutable and reproducible IRIs,
we were able to easily identify changes in the original data collection,
reducing up to 20x the amount of materialized triples
and their generation time when updating
a previously generated knowledge graph.
<!-- Conclusion   -->
Our preliminary results show the importance
of mechanisms to derive
unique and stable IRI strategies of data source updates,
to enable efficient Knowledge Graph generation pipelines.
<!-- Perspectives -->
In the future,
we will extend our approach to handle deletions in data collections,
and execute an extensive performance evaluation.</p>

    </div>
</section>

</header>

<main>
  <!-- Add sections by specifying their file name, excluding the '.md' suffix. -->
  <section>
<div datatype="rdf:HTML" property="schema:description">
      <!-- font size of codeblocks and inline code is huge -->
      <style>
code {
    font-size: 0.8em;
}
</style>

      <!-- Reduce figures and center them -->
      <style>
.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
}
</style>

      <h2 id="introduction">Introduction</h2>
      <!-- context -->
      <p>Knowledge Graphs usually coexist and integrate
with heterogenous and non-RDF data sources via mapping rules
<span class="references">[<a href="#ref-1">1</a>]</span>
<span class="references">[<a href="#ref-2">2</a>]</span>
<span class="references">[<a href="#ref-3">3</a>]</span>.
The result of executing such mapping rules is commonly a data dump
that represents a snapshot of the knowledge graph (KG) at a certain time.
However, in practice, knowledge graphs and their sources may evolve
making it difficult for data publishers to keep the KG up to date
and for consumers to stay synchronized with their latest version.
Recently, <a href="https://w3id.org/ldes/specification">Linked Data Event Stream (LDES)</a>
was introduced as a mechanism for publishers to expose
a stream of changes (additions, updates and deletions) of topic-based data sources,
e.g., sensor observations or versioned members,
as a collection of immutable members <span class="references">[<a href="#ref-4">4</a>]</span>.
Thanks to LDES, data consumers are able
to traverse and materialize historical versions,
or simply synchronize with the latest member changes of a data source
without having to re-download it in its full extent,
saving time, bandwidth, and computing resources.</p>

      <!-- What's the problem? -->
      <p>Efficient generation of an LDES from non-RDF sources
requires not only the capacity to define unique and immutable IRIs
that identify the individual member changes,
but also a mechanism that enables materializing only
the set of latest changes when processing new data source versions.
This needs also to consider the different strategies
data sources may use to publish updates (e.g. full data dumps or delta updates),
some of which may not provide ways to identify specific member versions.</p>

      <!-- Why is it an interesting problem? -->
      <p>Current KG generation approaches cannot leverage data source updates.
Instead, they regenerate the complete KG upon updates,
even when only a subset of the data source changes.
This approach becomes impractical for large data sources
where the cost of processing new updates
will grow proportionally to their update frequency
and may impact the operation of services relying on the resulting KG.
For example, by having to operate with outdated data
or even being taken offline while updates are processed.</p>

      <!-- What's my solution? -->
      <p>We address this problem by investigating how we can uniquely
and consistently (across multiple versions)
identify and materialize only member updates,
to avoid the costs of fully regenerating the KG.
We extend an RML processor implementation
to produce KGs modelled as LDES
and apply our approach over 3 different data sources
with varying characteristics, e.g. size and update strategy.
Our approach reduces the processing time and number of triples
up to 20x when a data source is updated.</p>

      <!-- What comes after my solution? -->
      <p>Through this work, we aim on reducing
the inherent (time and processing) costs
of continuous KG publishing that depend on
dynamic and evolving data sources,
while improving the overall operational behavior
of dependent services and applications.</p>

      <!-- Paper outline -->
      <p>In the <a href="#related-work">next section</a>, we present the related work.
<a href="#preliminaries">Section 3</a> introduces
the main concepts and technologies used within our approach.
In <a href="#approach">section 4</a> we describe in detail our approach and implementation.
<a href="#validation">Section 5</a> describes the validation of our approach
focusing on 3 use cases: bike sharing, public transport timetable, and weather data.
In the <a href="#conclusion">last section</a> we present our conclusions and discuss future work.</p>

    </div>
</section>

  <section id="related-work" inlist="" rel="schema:hasPart" resource="#related-work">
<div datatype="rdf:HTML" property="schema:description">
      <h2 property="schema:name">Related Work</h2>
      <!-- Mapping rules execution optimizations -->
      <p>Optimizing mapping rules execution for RDF KG generation
is an active research domain;
several approaches such as SDM-RDFizer <span class="references">[<a href="#ref-5">5</a>]</span>,
or FunMap <span class="references">[<a href="#ref-6">6</a>]</span> emerged
to optimize the mapping rules execution for generating RDF.
SDM-RDFizer avoids generating RDF
from duplicates in a data collection.
This way, all RDF triples are generated only once
every time the SDM-RDFizer is executed.
FunMap applies the same methodology to data transformations.
It executes every function on each collection member only once
and leverages an existing RML processor for the actual RDF generation.
However, both approaches cannot apply
this methodology across multiple versions of a data source.
If a subset of an KG must be updated,
it is fully regenerated.</p>

      <!-- Versioning side of things -->
      <p>Mechanisms to store and access versioned RDF data
include the approaches porposed by Ostrich <span class="references">[<a href="#ref-7">7</a>]</span>,
TailR <span class="references">[<a href="#ref-8">8</a>]</span>,
R&amp;Wbase <span class="references">[<a href="#ref-9">9</a>]</span>,
<a href="http://mementoweb.org/guide/rfc/">Memento</a> <span class="references">[<a href="#ref-10">10</a>]</span>,
or LDES <span class="references">[<a href="#ref-11">11</a>]</span>.
Three main RDF archive storage strategies can be identified:
(i) Independent Copies (IC),
(ii) Change-Based (CB),
and (iii) Timestamp-Based (TB).
Ostrich and TailR are both hybrid approaches,
combining all three strategies for efficient query operations.
R&amp;Wbase is based on <a href="https://git-scm.com">Git</a>
and applies a CB approach,
while Memento is a TB approach for accessing different versions over HTTP.
All these approaches put the burden of resolving versions on the producer.
LDES uses an IC approach by storing
complete and incrementally versioned objects
for each change in an append-only log.
This way, consumers can synchronize their collections with the producer,
and resolve the versioning on the consumer-side.
LDES is similar to the
Copy and Log approach <span class="references">[<a href="#ref-12">12</a>]</span>,
but can be applied using any versioning strategy,
not only limited to a timestamp-based approach.
Yet, timely generation of storage efficient LDES-based KGs
from dynamic non-RDF data sources remains an open challenge.</p>

    </div>
</section>

  <section id="preliminaries" inlist="" rel="schema:hasPart" resource="#preliminaries">
<div datatype="rdf:HTML" property="schema:description">
      <h2 property="schema:name">Preliminaries</h2>
      <!-- RDF Mapping Language -->

      <p><strong>RDF Mapping Language (RML)</strong> <span class="references">[<a href="#ref-13">13</a>]</span> broadens
<a href="https://www.w3.org/TR/r2rml">R2RML</a>’s scope 
and covers mapping rules from data in different (semi-)structured formats, 
e.g., CSV, XML, JSON which define how heterogeneous data is transformed in RDF.
RML has support for data transformations through its alignment
with the Function Ontology (FnO) <span class="references">[<a href="#ref-14">14</a>]</span>.</p>

      <figure id="listing1" class="listing">
<pre><code>&lt;#Mapping&gt; a rr:TriplesMap; rml:LogicalSource &lt;#InputX&gt;;
</code><code>  rr:subjectMap [ rr:template &quot;http://ex.com/{ID}&quot;; rr:class foaf:Person; 
</code><code>    rml:logicalTarget &lt;#Target&gt; ];
</code><code>&lt;#Target&gt; a rmlt:LogicalTarget;
</code><code>  rmlt:target [ a void:Dataset; void:dataDump &lt;file:///dump.nt.gz&gt; ];
</code><code>  rmlt:serialization formats:N-Triples; rmlt:compression comp:gzip .
</code></pre>

<figcaption>
          <p><span class="label">Listing 1:</span> RML Logical Target definition referenced in a RML Triples Map.</p>
        </figcaption>
</figure>

      <p>Recently, RML was extended with 
RML’s Logical Target <span class="references">[<a href="#ref-15">15</a>]</span>
to specify where generated RDF triples must be exported to.
Each triple can be exported to one or multiple targets.
Logical Targets (<a href="#listing1">Listing 1</a>: lines 4-6) are described
in a similar manner as a RML Logical Source.
Each Logical Target has a <code>rmlt:target</code> property (line 5)
to specify
<a href="https://rml.io/specs/dataio/">how a target should be accessed</a> such as
<code>void:Dataset</code>, or <code>sd:Service</code>.
The serialization format (default <em>N-Quads</em>, line 6)
and compression (default <em>none</em>, line 6) are optional.
Each Logical Target can be 
<a href="https://rml.io/specs/rml-target">referenced in any RML Term Map</a>
to control where each triple is exported to.</p>

      <!-- Linked Data Event Streams -->
      <p><strong>Linked Data Event Streams (LDES)</strong></p>

      <figure id="listing2" class="listing">

<pre><code>&lt;#Collection&gt; a ldes:EventStream;
</code><code>  ldes:timestampPath sosa:resultTime ; ldes:versionOfPath dcterms:isVersionOf ;
</code><code>  tree:shape ex:SHACLShape ; tree:view &lt;#Page1&gt; ;
</code><code>  tree:member [ a sosa:Observation;
</code><code>    sosa:resultTime &quot;2021-01-01T00:00:00Z&quot;^^xsd:dateTime ;
</code><code>    sosa:hasSimpleResult &quot;42&quot;^^xsd:integer ; ] ; .
</code><code>&lt;#Page1&gt; a tree:Node ;
</code><code>  tree:relation [ a tree:GreatherThanOrEqualToRelation ;
</code><code>    tree:path sosa:resultTime ; tree:node &lt;#Page2&gt; ;
</code><code>    tree:value &quot;2020-12-24T12:00:00Z&quot;^^xsd:dateTime ; ];
</code><code>  ldes:retentionPolicy [ a ldes:DurationAgoPolicy
</code><code>    tree:value “P1Y”^^xsd:duration; # Keep 1 year of data ]; .
</code></pre>

<figcaption>
          <p><span class="label">Listing 2:</span> Linked Data Event Streams definitions.</p>
        </figcaption>
</figure>

      <p>A <a href="https://w3id.org/ldes/specification">Linked Data Event Stream</a>
is a collection of immutable members such as 
versioned entities, observations, etc. in RDF.
A Linked Data Event Stream (<code>ldes:EventStream</code>, <a href="#listing2">Listing 2</a>: line 1)
relies on the <a href="https://w3id.org/tree/specification">TREE specification</a>
for describing collection and pagination characteristics.
Each LDES has a <code>tree:shape</code> (line 3)
which describes a SHACL shape defining
each member in the collection, and <code>tree:member</code>s (lines 4-6)
indicating the members of a collection.
Optionally, one or multiple path descriptions (line 2)
such as <code>ldes:timestampPath</code> or <code>ldes:versionOfPath</code> may be provided
to indicate the relation with previous versions of a member.
An LDES may provide different indexing strategies through the
<code>tree:view</code> (line 3) and <code>tree:relation</code> (lines 8-10) properties,
leveraged from the TREE specification.
Additionally an LDES may specify a retention policy (lines 11-12)
such as <code>ldes:LatestVersionSubset</code>.</p>

    </div>
</section>

  <section id="approach" inlist="" rel="schema:hasPart" resource="#approach">
<div datatype="rdf:HTML" property="schema:description">
      <h2 property="schema:name">Approach</h2>

      <p>In this section, we describe our RML- and FnO-based approach
to generate unique and reproducible IRIs
for collection’s member updates,
which enables continuous KG generation modelled as a LDES.
We discuss how we generate identifiers for changes
of a collection’s member,
and how we expose these changes through a LDES.</p>

      <h3 id="data-collection-types">Data collection types</h3>

      <figure id="table1" class="table">

        <table>
          <thead>
            <tr>
              <th>History</th>
              <th>Immutable</th>
              <th>Description</th>
              <th>Use case</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Latest state</td>
              <td>Yes</td>
              <td>Dump of all collection members with an unique version identifier</td>
              <td><a href="https://blue-bike.be">BlueBike bike sharing</a></td>
            </tr>
            <tr>
              <td>Latest state</td>
              <td>No</td>
              <td>Dump of all collection members without an unique version identifier</td>
              <td><a href="https://www.belgiantrain.be/nl/3rd-party-services/mobility-service-providers/public-data/">NMBS public transport timetable (GTFS)</a></td>
            </tr>
            <tr>
              <td>Latest changes</td>
              <td>Yes</td>
              <td>Dump of collection members which are updated with an unique version identifier</td>
              <td><a href="https://wiki.openstreetmap.org/wiki/Planet.osm/diffs">OpenStreetMap Planet.osm diffs</a></td>
            </tr>
            <tr>
              <td>Latest changes</td>
              <td>No</td>
              <td>Dump of collection members which are updated without an unique version identifier</td>
              <td><a href="https://www.belgiantrain.be/nl/3rd-party-services/mobility-service-providers/public-data/">NMBS public transport timetable updates (GTFS-RT)</a></td>
            </tr>
            <tr>
              <td>Full history</td>
              <td>Yes</td>
              <td>Dump of all versions of all collection members with an unique version identifier</td>
              <td><a href="https://opendata.meteo.be/">KMI weather sensors</a></td>
            </tr>
          </tbody>
        </table>

        <figcaption>
          <p><span class="label">Table 1:</span> Different data source types exist regarding immutability 
and history.
A collection is considered immutable if all its members 
and their corresponding updates can be uniquely identified 
through dedicated properties e.g., update timestamps.
We validate each possibility for history and immutability through our use cases.</p>
        </figcaption>
      </figure>

      <p>This section discusses the various data collection types 
we investigate in this paper. 
We identify 5 different types based on history and immutability (Table 1).</p>

      <p>A collection’s member is considered immutable
if it provides a unique identifier
for each change in any its properties.
This way, data consumers can identify
each data change individually without comparing the data against previous versions.
We consider 2 categories:
(i) immutable,
and (ii) mutable data collections.
The BlueBike bike sharing data source can be considered as an immutable collection
given that it provides a timestamp
indicating when each member was last updated.
In contrast, the public transport timetable data source
is a mutable collection as it does not provide such property.
Therefore, the members in the public transport timetable data
must be compared with their previous version by the consumer.</p>

      <p>Some collections provide the history of their members
such as the KMI weather data source
which allows data consumers
to retrieve (all) changes applied to the members in a collection.
However, publishers may choose to not publish the complete member history
e.g. last 30 days.
We consider 4 different categories:
(i) latest state,
(ii) latest changes,
and (iii) historical collections.
Collections which offer historical versions of their members
are automatically immutable
since their historical identifiers can be used
to uniquely identify a member’s version.
BlueBike bike sharing and NMBS public transport timetable data do not provide
any historical data,
it is up to the consumer to store each version
of the members in the collection to keep their history.</p>

      <h3 id="unique-and-reproducible-iris">Unique and reproducible IRIs</h3>

      <figure id="figure1">
<img src="figures/figure1.svg" alt="[Figure 1]" class="center" style="width:60%;" />
<figcaption><span class="label">Fig. 1:</span> 
Unique and reproducible IRIs are generated by observing properties
of collection&#8217;s member for changes.
If necessary, additional properties are added 
to make the IRI unique for a member update.
</figcaption>
</figure>

      <p>In this section,
we introduce an algorithm to generate unique and reproducible IRIs
by observing the properties of the data source.
These properties may already be sufficient to generate
an unique and reproducible IRI.
If that is not the case,
these properties are compared against the last known version
to generate an IRI, only if the data source was updated.
This way, we can uniquely identify changes
and avoid duplicates.</p>

      <!-- Algorithm description -->
      <p>We will illustrate our algorithm (<a href="#figure1">Figure 1</a>)
through an example of BlueBike bike sharing station data source,
which provides all stations’ data through a JSON Web API.
We observe the properties <code>bikes_available</code>, <code>bikes_in_use</code>, <code>last_seen</code>
since these properties are updated
when a bike is rented or returned to the station.</p>

      <!-- Case A: Collection's member contains properties for unique and reproducible IRIs -->
      <p>The <code>last_seen</code> property is unique for each change in the collection’s member,
it contains the timestamp when this member was updated in the collection.
This property is leveraged to generate unique and reproducible IRIs
such as <code>http:/​/​ex.org/station/48#2022-02-14T13:47:07</code>.
When the data is fetched again from the BlueBike API,
it will generate the same IRI which already exists
in the current version of the knowledge graph.
The generation of the RDF for the member will be skipped in this case.
If one of the member properties were updated,
the generated IRI will differ from the existing one in the knowledge graph,
and a generation is performed.</p>

      <!-- Case B: Collection's member  needs a diff checker -->
      <p>If the member does not contain unique properties for each change
such as <code>last_seen</code> (<a href="#listing3">Listing 3</a>, line 9),
the observed properties are compared with the last known value.
If one of these values differ,
we calculate the current timestamp and add it to the IRI template
before generating an unique and reproducible IRI for the member.</p>

      <h3 id="ldes-logical-target">LDES Logical Target</h3>

      <p>This section introduces our LDES Logical Target which is used
to export the generated RDF as an LDES to a file.
Thanks to the modularity of
RML’s Logical Target <span class="references">[<a href="#ref-15">15</a>]</span>,
we could add an LDES Logical Target to any RML processor.
This way, we can re-use existing tools from the LDES ecosystem on this LDES.</p>

      <p>An LDES Logical Target is a regular RML Logical Target
with an additional <code>ldes:EventStreamTarget</code> type.
Each <code>ldes:EventStreamTarget</code> adds LDES specific properties
to a regular Logical Target
such as LDES paths (line 6) or
a <a href="https://www.w3.org/TR/shacl/">SHACL</a> shape (line 7)
to specify how consumers can traverse the LDES
and describe the structure of a member.
This way, an LDES Logical Target only adds metadata to serialized output.</p>

      <figure id="listing3" class="listing">

<pre><code>&lt;#LDESToFile&gt; a rmlt:LogicalTarget;                 
</code><code>  rmlt:target [ a ldes:EventStreamTarget;           
</code><code>    dcat:distribution [ a dcat:Distribution; 
</code><code>      dcat:accessURL &lt;file://data/dump.nq.zip&gt;; ]; ];                                                
</code><code>  rmlt:serialization formats:N-Quads; rmlt:compression comp:zip;                        
</code><code>  ldes:timestampPath sosa:resultTime ; ldes:versionOfPath dcterms:isVersionOf ;        
</code><code>  tree:shape &lt;http://example.org/shape.shacl&gt; ; .
</code></pre>

<figcaption>
          <p><span class="label">Listing 3:</span> LDES Logical Target definition in RML to export an LDES to a file.</p>
        </figcaption>
</figure>

    </div>
</section>

  <section id="validation" inlist="" rel="schema:hasPart" resource="#validation">
<div datatype="rdf:HTML" property="schema:description">
      <h2 property="schema:name">Validation</h2>
      <p>In this section we describe how we validated our approach
through 3 use cases:
(i) BlueBike bike sharing,
(ii) NMBS public transport timetable,
and (iii) KMI weather sensors data
with the RMLMapper as RML processor.</p>

      <p>We implemented our approach in the RMLMapper,
but it can be implemented in any RML processor since
we re-use existing features such as an FnO function
to generate unique and reproducible IRIs;
and RML’s Logical Target for producing LDES-based KGs.
Our <a href="https://github.com/RMLio/RML-LDES-mapping-rules">mapping rules</a>
and 
<a href="https://github.com/RMLio/rmlmapper-java/releases/tag/v5.0.0">implementation</a>
are available on GitHub.</p>

      <h3 id="use-cases">Use cases</h3>
      <p><strong>BlueBike bikesharing data</strong>
The <a href="https://www.blue-bike.be/">BlueBike</a>
bike sharing data is available through a JSON Web API
and provides a dump of all stations and the current available bikes.
We access the Blue Bike Web API in the RML mappings
through the W3C Web of Things Web API support
in a RML Logical Source <span class="references">[<a href="#ref-15">15</a>]</span>.
These data are mapped using a
<a href="https://github.com/pietercolpaert/Blue-Bike-to-Linked-GBFS">GBFS-based vocabulary</a>
into RDF,
and exported to a file as an LDES with our LDES Logical Target.
Since the Web API does not provide an interface to retrieve
updates, we poll the Web API every minute.
During polling, we generate unique and reproducible IRIs
with our algorithm (<a href="#unique-and-reproducible-iris">section 4.2</a>)
by leveraging the <code>last_seen</code> 
property of the data source.
If a member is updated, we regenerate the member’s RDF.</p>

      <p><strong>NMBS Public transport timetable data</strong>
The Belgian railway agency <a href="https://www.belgiantrain.be">NMBS/SNCB</a>
publishes its yearly timetable schedule
as a <a href="https://gtfs.org">GTFS</a> dump.
Every day they republish the whole schedule
without providing data consumers a list of changes.
Moreover, they do not provide any properties which can be leveraged
to generate unique and reproducible IRIs.
We download the GTFS dump daily to check for changes.
If a change is found,
we generate an unique and reproducible IRI for the member
by adding a timestamp.
We use the time when the mapping rules are executed
but any property can be used here,
as long as the IRI becomes unique and reproducible.</p>

      <p><strong>KMI weather sensors data</strong>
The Belgian meteorological institute <a href="https://kmi.be">KMI</a>
provides measurements of
their <a href="https://opendata.meteo.be/">Automatic Weather Stations</a> as a CSV dump.
We use this data to validate our approach given that it provides 
the sensors’ history with unique identifiers.
We leveraged these identifiers to generate our unique IRIs.</p>

      <h3 id="preliminary-results">Preliminary results</h3>
      <p>Our approach generates knowledge graphs
which are 5-20x smaller and faster
than fully regenerating a knowledge graph.
Our preliminary results for the BlueBike use case indicate
that the knowledge graph generation time and size 
is reduced by 5-10x,
while for the other use cases, we saw a reduction of 20x.</p>

    </div>
</section>

  <section id="conclusion" inlist="" rel="schema:hasPart" resource="#conclusion">
<div datatype="rdf:HTML" property="schema:description">
      <h2 property="schema:name">Conclusion</h2>
      <p>Generating unique and reproducible IRIs for every collection’s member
avoids regenerating the whole knowledge graph when the collection is updated.
Exposing these updates as a LDES provides data consumers an interface
that facilitates data synchronization tasks.
In this paper, we
(i) introduce an algorithm for generating unique and reproducible IRIs
from different types of data sources,
and (ii) a RML Logical Target to export dataset updates as a LDES.
We validated our approach through 3 use cases
to indicate that our approach is use case independent.
In the future, we aim to extend our approach to handle deletions
and perform an performance evaluation.</p>

    </div>
</section>

</main>

<footer><section>
<h2 id="references">References</h2>
<dl class="references">
  <dt id="ref-1">[1]</dt>
  <dd resource="#Dimou2017PhDBook" typeof="schema:Thesis">Dimou, A.: High Quality Linked Data Generation from Heterogeneous Data (2017).</dd>
  <dt id="ref-2">[2]</dt>
  <dd resource="https://pieterheyvaert.com/phd/dissertation" typeof="schema:Thesis">Heyvaert, P.: Improving Effectiveness of Knowledge Graph Generation Rule Creation and Execution. <a href="https://pieterheyvaert.com/phd/dissertation">https:/​/​pieterheyvaert.com/phd/dissertation</a> (2019).</dd>
  <dt id="ref-3">[3]</dt>
  <dd resource="http://oa.upm.es/67890/" typeof="schema:Thesis">Fraga, D.C.: Knowledge Graph Construction from Heterogeneous Data Sources exploiting Declarative Mapping Rules. <a href="http://oa.upm.es/67890/">http:/​/​oa.upm.es/67890/</a> (2021).</dd>
  <dt id="ref-4">[4]</dt>
  <dd resource="https://link.springer.com/chapter/10.1007/978-3-030-74296-6_3" typeof="schema:Article">Van Lancker, D., Colpaert, P., Delva, H., Van de Vyvere, B., Rojas Meléndez, J., Dedecker, R., Michiels, P., Buyle, R., De Craene, A., Verborgh, R.: Publishing base registries as Linked Data Event Streams. In: Proceedings of the 21th International Conference on Web Engineering. pp. 28–36. Springer (2021).</dd>
  <dt id="ref-5">[5]</dt>
  <dd resource="#Iglesias2020SDMRDFizer" typeof="schema:Article">Iglesias, E., Jozashoori, S., Chaves-Fraga, D., Collarana, D., Vidal, M.-E.: SDM-RDFizer: An RML Interpreter for the Efficient Creation of RDF Knowledge Graphs. In: Proceedings of the 29th ACM International Conference on Information &amp; Knowledge Management (2020).</dd>
  <dt id="ref-6">[6]</dt>
  <dd resource="#jozashoori2020funmap" typeof="schema:Article">Jozashoori, S., Chaves-Fraga, D., Iglesias, E., Vidal, M.-E., Corcho, O.: FunMap: Efficient Execution of Functional Mappings for Knowledge Graph Creation. In: International Semantic Web Conference. pp. 276–293 (2020).</dd>
  <dt id="ref-7">[7]</dt>
  <dd resource="https://project-hobbit.eu/wp-content/uploads/2018/05/MOCHA_Paper_1.pdf" typeof="schema:Article">Taelman, R., Vander Sande, M., Verborgh, R.: Versioned Querying with OSTRICH and Comunica in MOCHA 2018. In: Proceedings of the Mighty Storage Challenge (2018).</dd>
  <dt id="ref-8">[8]</dt>
  <dd resource="https://doi.org/10.1145/2814864.2814875" typeof="schema:Article">Meinhardt, P., Knuth, M., Sack, H.: TailR: A Platform for Preserving History on the Web of Data. In: Proceedings of the 11th International Conference on Semantic Systems. pp. 57–64. Association for Computing Machinery (2015).</dd>
  <dt id="ref-9">[9]</dt>
  <dd resource="http://ceur-ws.org/Vol-996/papers/ldow2013-paper-01.pdf" typeof="schema:Article">Vander Sande, M., Colpaert, P., Verborgh, R., Coppens, S., Mannens, E., Van de Walle, R.: R&amp;Wbase: git for triples. In: Proceedings of the 6th Workshop on Linked Data on the Web (2013).</dd>
  <dt id="ref-10">[10]</dt>
  <dd resource="#Sompel2010AnHV" typeof="schema:Article">de Sompel, H.V., Sanderson, R., Nelson, M.L., Balakireva, L., Shankar, H., Ainsworth, S.: An HTTP-Based Versioning Mechanism for Linked Data. ArXiv. abs/1003.3661, (2010).</dd>
  <dt id="ref-11">[11]</dt>
  <dd resource="https://link.springer.com/chapter/10.1007/978-3-030-74296-6_3" typeof="schema:Article">Van Lancker, D., Colpaert, P., Delva, H., Van de Vyvere, B., Meléndez, J.R., Dedecker, R., Michiels, P., Buyle, R., De Craene, A., Verborgh, R.: Publishing Base Registries as Linked Data Event Streams. Presented at the (2021).</dd>
  <dt id="ref-12">[12]</dt>
  <dd resource="https://doi.org/10.1145/319806.319816" typeof="schema:Article">Salzberg, B., Tsotras, V.J.: Comparison of Access Methods for Time-Evolving Data. ACM Comput. Surv. 158–221 (1999).</dd>
  <dt id="ref-13">[13]</dt>
  <dd resource="http://ceur-ws.org/Vol-1184/ldow2014_paper_01.pdf" typeof="schema:Article">Dimou, A., Vander Sande, M., Colpaert, P., Verborgh, R., Mannens, E., Van de Walle, R.: RML: A Generic Language for Integrated RDF Mappings of Heterogeneous Data. In: Proceedings of the 7th Workshop on Linked Data on the Web (2014).</dd>
  <dt id="ref-14">[14]</dt>
  <dd resource="https://doi.org/10.1016/j.future.2019.10.006" typeof="schema:Article">De Meester, B., Seymoens, T., Dimou, A., Verborgh, R.: Implementation-independent Function Reuse. Future Generation Computer Systems. 946–959 (2020).</dd>
  <dt id="ref-15">[15]</dt>
  <dd resource="https://dylanvanassche.be/assets/pdf/icwe2021-wot-logical-target.pdf" typeof="schema:Article">Van Assche, D., Haesendonck, G., De Mulder, G., Delva, T., Heyvaert, P., De Meester, B., Dimou, A.: Leveraging Web of Things W3C Recommendations for Knowledge Graphs Generation. In: Web Engineering, 21st International Conference, ICWE 2021, Proceedings. pp. 337–352 (2021).</dd>
</dl>
</section>
</footer>



</body>
</html>
