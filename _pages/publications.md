---
layout: page
permalink: /publications/
title: Publications
description: My research articles and other publications, chronological ordered.
years: [2022, 2021, 2020, 2019]
---

{% for y in page.years %}
  <h3 class="year">{{y}}</h3>
  {% bibliography -f papers -q @*[year={{y}}]* %}
{% endfor %}

{% include kg/publications.html %}
