---
layout: post
title:  DIY NMBS Shimmns freight cars
date:   2020-04-23 12:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: Designing and 3D printing NMBS shimms freight cars in H0 (1:87)
---

I always wanted to have some NMBS B Cargo freight cars to transport steel on my
model railroad.
If you have ever been in Belgium, you may already saw those passing by at a 
station.
These freight cars are pretty common and go by the name *shimmns*.

Several model railroad manufacturers already developed H0 models of these cars.
However, I wanted to have a challenge and design one myself :muscle:!

### Design

I use Autodesk's [Fusion 360](https://www.autodesk.com/products/fusion-360/students-teachers-educators) 
as my design software because I'm familiar with the design process it uses and 
the free license for hobby and academic purposes.
However, the software is closed-source and locks you into the Autodesk 
ecosystem.
I tried [FreeCAD](https://www.freecadweb.org/) in the past, but I haven't been 
able to get the same results with it as with Fusion 360.

It's impossible to design the freight car in one piece.
You make mistakes on the way and want to redesign some parts later on.
Because of this, I design each part separately.

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-1.png">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-2.png">
    Each part of the freight car is designed separately.
</div>

Once I designed several parts, I create an assembly.
An assembly is a design file where you import several parts and assemble those
together.
Moreover, you can design your parts in such way that they are 3D printing 
friendly while you can check in the assembly if they still fit together 
:smiley:.

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-3.png">
    An assembly of my NMBS B Cargo Shimmns freight car.
</div>

Designing and testing such cars takes weeks or months to get them right 
:open_mouth:.
Although, once they are finished, you can be very proud of your achievement 
:smile:.

### 3D printing

I print my model railroad designs with a 3D printer.
I have a [Velleman K8400](https://vertex3dprinter.eu/producten/vertex/) at my 
disposal where I upgraded the bed to a heated one and an 
[E3Dv6 extruder](https://e3d-online.com/v6-all-metal-hotend).

The Shimmns freight car consists of dozen of 3D printed parts.
Not only the chassis is 3D printed, but also buffers and other smaller parts.
These smaller parts sometimes require custom support which is removed after
printing.

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-4.jpg">
    3D printed parts of my Shimmns freight car. Smaller parts such as buffers 
    have their own custom support which is removed afterwards.
</div>

They say: *"Three times a charm"*, that's the same for my prints most of the 
time.
It takes several iterations to get your design and prints right.
You have to take tolerance and other factors into account, while the model 
still have to be functional.
However, you always learn something from a failed print which you can avoid 
when designing your next model!

### Assemble time!

The best part is of course the assembling!
It's always a great feeling when you assembly your design and it just fit 
:smiley:.

The freight car's canvas skeleton is first assembled. 
The skeleton is used to support the blue canvas of the freight car.
It consists of several ribs which are separately printed and assembled as one
skeleton.

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-5.jpg">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-6.jpg">
    Assembling the skeleton to support the blue canvas of the freight car. 
</div>

The canvas is created from tissues and thinned wood glue with water (50/50).

1. The tissue is flatted with the paint roller. The paint roller makes sure 
that tissue is soaked with the wood glue mix
2. The tissue is placed over the skeleton and tighted by hand.
3. Let it dry for a whole day. 
The tissue will become hard because of the glue, perfectly for painting!

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-7.jpg">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-8.jpg">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-9.jpg">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-10.jpg">
    Creating a canvas from a tissue with a wood glue mix. 
</div>

I painted the whole model with a cheap paint spray can.
It's important to first paint the PLA plastic of the model with a primer.
Afterwards, 2 layers of the final colour is sufficient to paint your model.

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-11.jpg">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-12.jpg">
    Painting the model with a cheap paint spray can.
</div>

Details are important when building a scale model.
I created a mold to create the handle and other parts of the model.
The mold is 3D printed and allows me to reproduce these smaller parts of each
model in the same.

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-13.jpg">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-14.jpg">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-15.jpg">
    Handles and other smaller parts are created using a 3D printed mold.
</div>

Last but not least, the decals are added to the model as a final touch!
It's a precision work, but they make a big difference for a model.

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-16.jpg">
    <img class="col three" src="{{ site.baseurl }}/assets/img/shimmns-17.jpg">
    Decals and final details are added.
</div>
