---
layout: post
title:  Nitrokey as GPG smartcard in Linux
date:   2021-02-10 12:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: Never lose your GPG keys again
---

### Nitrokey, an affordable GPG smartcard

For years, I use GPG to encrypt my passwords and e-mails.
It's FOSS and, easy to use and works in text based environments as well.
However, exchanging GPG keys between machines is a tedious process 
and exposes your private key during the transfer.
To solve this, I bought a [Nitrokey Start](https://nitrokey.com) 
to store my GPG keys.

While alternatives such as Yubico and others are available, 
I picked Nitrokey since:

1. They are committed to FOSS
2. Germany based manufacturing. 
Not only support I labor in the EU, 
but the production itself is also better protected by EU laws 
and standards than US based companies.
3. Several [tutorials and guides](https://www.nitrokey.com/documentation/applications) 
are available on their website.
5. It's fairly cheap to start with Nitrokey Start.

### Installing Nitrokey Start on Alpine Linux

Even though Nitrokey does not provide a specific Alpine Linux guide, 
but one for Debian based systems, it is fairly easy to get it working.
I suggest to read [Nitrokey's tutorial](https://www.nitrokey.com/documentation/installation#p:nitrokey-start&os:linux) 
before following the short guide below.
The main difference is the first step where I used Alpine Linux's 
package manager instead of Debian's one.

1. Install `scdaemon` and `gpg`: `sudo apk add gnupg gnupg-scdaemon pinentry-gnome`.
2. Insert the Nitrokey in an USB port
3. Check if GPG can see your Nitrokey with: `gpg --card-status`. 
This command should print all the available information of the Nitrokey 
such as Reader ID, Serial number, etc.
If GPG can't see your Nitrokey, you might need to add some `udev` rules first, 
as explained 
[here](https://www.nitrokey.com/documentation/installation#Troubleshooting).
4. Generate new GPG keys or transfer existing keys to the Nitrokey, 
they provide a [great tutorial on their website](https://www.nitrokey.com/documentation/openpgp-email-encryption): 
5. Change the admin PIN using `gpg --card-edit` with the commands 
`admin` and `passwd`. Do not change the user PIN yet! 
Once the admin PIN is changed, repeat this step and change the user PIN.
6. You can also set `url`, `lang`, `salutation`, `name` when editing 
the Nitrokey with `gpg --card-edit`.
If you have uploaded your public key online, you can set the URL to point 
to your public key online.
This way, you can use `fetch` in GPG on other machines 
to retrieve the public key of the Nitrokey fairly easy.

### Getting SSH authentication to work

The Nitrokey tutorial will give you 3 keys:

- Sign key
- Encrypt key
- Authentication key

We will use the last one to configure GPG as an SSH agent.
This way, we can also authenticate against SSH servers with our Nitrokey.

1. Configure GPG to run as an agent:
```
# Add to ~/.gnupg/gpg.conf
use-agent
```
2. Enable GPG SSH support with GNOME pinentry program:
```
# Add to ~/.gnupg/gpg-agent.conf
enable-ssh-support
pinentry-program /usr/bin/pinentry-gnome3
```
3. Configure SSH to use GPG as SSH agent:
```
# Add to ~/.profile or ~/.bashrc if you're using bash
export GPG_TTY=$(tty)
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
```
4. Specify which GPG key may be exported when GPG is acting as SSH agent:
```
# Find keygrip of your GPG key
gpg --with-keygrip -k $EMAIL_ADDRESS_LINKED_WITH_GPG_KEY

# Extract the keygrip value and add it to
vim ~/.gnupg/sshcontrol
```

5. Kill GPG: `pkill gpg-agent`
6. Get you SSH key: `ssh-add -L`. This should print your SSH key using your Nitrokey.
7. Add your new SSH key to your favourite git hosting provider

### Signing git commits

Now that we have a GPG identity, 
we can use it's sign key to sign git commits as well!

1. List your keys and get the key ID of the key with `S` in it's capabilities:
```
gpg --list-secret-keys --keyid-format LONG
```
2. Tell git about your signing key:
```
git config --global user.signingkey <KEY ID>
```
3. If you want to sign commits, you need to add `-S` to `git commit` 
or enable auto signing with `git config --global commit.gpgsign true` 
