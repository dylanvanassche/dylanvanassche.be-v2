---
layout: post
title:  Time flies even when you aren't having fun
date:   2022-04-01 09:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: Life sucks sometimes
---

2 years ago I received some horrible news,
my best friend [Ellen](https://dylanvanassche.be/blog/2020/goodbye-my-friend/)
suddenly passed away.
She was a hell of a fighter and always there when you needed her.

We had a special bound that I will never find again.
Even if we haven't seen each other for a while,
we picked up where we left of, as it was yesterday.
True friendship :heart:

Life sucks sometimes really hard, this is one of those moments.
While others are trolling people for April's fool day,
this day is for me really the opposite.
So if I don't appreciate your April's fool day joke,
it has nothing to do with you or your joke probably.

Rest in peace Ellen, you will never be forgotten.
