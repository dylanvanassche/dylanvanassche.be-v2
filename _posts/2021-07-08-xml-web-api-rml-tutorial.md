---
layout: post
title:  "Generate RDF from an XML Web API"
date:   2021-07-08 12:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: Convert your XML Web API to RDF with RML!
redirect: https://rml.io/docs/rml/tutorials/wot-xml/
---

This tutorial is hosted on the RML website, if your browser doesn't redirect 
you automatically, you can click on this link: 
[https://rml.io/docs/rml/tutorials/wot-json/](https://rml.io/docs/rml/tutorials/wot-xml/)
<script>
    location.replace("https://rml.io/docs/rml/tutorials/wot-xml/");
</script>
